package com.example.qanda.Service.AdminSer;

import com.example.qanda.Entity.Admin.Admin;
import com.example.qanda.Entity.Admin.AdminDto;
import com.example.qanda.Exception.Admin.EmailDuplicate;

import java.util.List;

public interface AdminService {

    Admin addAdmin(AdminDto adminDto) throws EmailDuplicate;

    List<Admin> getAllAdmin();

    Admin getAdminById(Long id);

    String login(AdminDto adminDto) throws RuntimeException;

    Admin editUser(AdminDto adminDto, Long id);

    void deleteAdmin(Long id);

    boolean checkIfUsernameExist(String username);

    boolean checkIfEmailExist(String email);
}
