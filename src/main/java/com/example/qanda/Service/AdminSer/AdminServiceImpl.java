package com.example.qanda.Service.AdminSer;

import com.example.qanda.Entity.Admin.Admin;
import com.example.qanda.Entity.Admin.AdminDto;
import com.example.qanda.Entity.Admin.AdminRepo;
import com.example.qanda.Exception.Admin.EmailDuplicate;
import com.example.qanda.Exception.Admin.UnknownUserException;
import com.example.qanda.Exception.Admin.UserNotAlreadyException;
import com.example.qanda.Exception.Admin.UsernameDuplicate;
import com.example.qanda.Security.TokenAuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminServiceImpl implements AdminService {

    @Autowired
    AdminRepo adminRepo;

    String a = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" + "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" + "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" + "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" + "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" + "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";


    @Override
    public Admin addAdmin(AdminDto adminDto) throws EmailDuplicate {

        if (checkIfUsernameExist(adminDto.getUsername())) {
            throw new UsernameDuplicate();
        }
        if (checkIfEmailExist(adminDto.getEmail())) {
            throw new EmailDuplicate();

        } else {
            Admin a = new Admin();
            a.setUsername(adminDto.getUsername());
            a.setFullname(adminDto.getFullname());
            a.setPassword(adminDto.getPassword());
            a.setEmail(adminDto.getEmail());
            a.setGoogle_id(adminDto.getGoogle_id());
            a.setThumbnail(adminDto.getThumbnail());
            return adminRepo.save(a);
        }

    }

    @Override
    public boolean checkIfUsernameExist(String username) {
        return adminRepo.findByUsername(username) != null;
    }

    @Override
    public boolean checkIfEmailExist(String email) {
        return adminRepo.findByEmail(email) != null;
    }


    @Override
    public List<Admin> getAllAdmin() {
        return (List<Admin>) adminRepo.findAll();
    }

    @Override
    public Admin getAdminById(Long id) {
        return adminRepo.findById(id).get();
    }


    @Override
    public String login(AdminDto adminDto) throws RuntimeException {
        Admin user = adminRepo.findByUsername(adminDto.getUsername());


        if (user == null) {
            throw new UnknownUserException();

        }
        if (adminDto.getPassword().equals(user.password)) {
            return TokenAuthenticationService.addAuthentication(user.getUsername(), a);

        } else {
            throw new UnknownUserException();
        }

    }


    @Override
    public Admin editUser(AdminDto adminDto, Long id) {
        Admin admin = adminRepo.findById(id).orElseThrow(UserNotAlreadyException::new);
        admin.setUsername(adminDto.getUsername());
        admin.setFullname(adminDto.getFullname());
        admin.setPassword(adminDto.getPassword());
        admin.setEmail(adminDto.getEmail());
        admin.setGoogle_id(adminDto.getGoogle_id());
        adminDto.setThumbnail(adminDto.getThumbnail());
        return adminRepo.save(admin);

    }

    @Override
    public void deleteAdmin(Long id) {
        adminRepo.deleteById(id);
    }

}
