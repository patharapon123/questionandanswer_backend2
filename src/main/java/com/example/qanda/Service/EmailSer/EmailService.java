package com.example.qanda.Service.EmailSer;

import com.example.qanda.Entity.EmailDto.MailRequest;
import com.example.qanda.Entity.EmailDto.MailResponse;

import java.util.Map;

public interface EmailService {

    MailResponse sendEmailAddAnswer(MailRequest request, Map<String, Object> model);

    MailResponse sendEmailEditAnswer(MailRequest request, Map<String, Object> model);

}
