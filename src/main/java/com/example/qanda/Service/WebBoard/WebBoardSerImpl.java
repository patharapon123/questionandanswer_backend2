package com.example.qanda.Service.WebBoard;

import com.example.qanda.Entity.WebBoard.WebBoard;
import com.example.qanda.Entity.WebBoard.WebBoardDto;
import com.example.qanda.Entity.WebBoard.webBoardRepo;
import com.example.qanda.Exception.Question.QuestionNotAlreadyException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class WebBoardSerImpl implements WebBoardSer {


    @Autowired
    webBoardRepo webBoardRepo;

    @Override
    public WebBoard addQuestion(WebBoardDto webBoardDto) {
        WebBoard Uq = new WebBoard();

        Uq.setQuestion(webBoardDto.getQuestion());
        Uq.setQuestionDate((webBoardDto.getQuestionDate()));
        Uq.setUpdater_user(webBoardDto.getUpdater_user());
        Uq.setIsPublic(webBoardDto.getIsPublic());


        return webBoardRepo.save(Uq);
    }

    @Override
    public WebBoard addAnswer(WebBoardDto webBoardDto, Long id) {

        WebBoard userQuestion = webBoardRepo.findById(id).orElseThrow(QuestionNotAlreadyException::new);

        userQuestion.setAnswer(webBoardDto.getAnswer());
        userQuestion.setAnswerDate(webBoardDto.getAnswerDate());
        userQuestion.setUpdater_admin(webBoardDto.getUpdater_admin());
        userQuestion.setIsPublic(webBoardDto.getIsPublic());
        return webBoardRepo.save(userQuestion);
    }

    @Override
    public WebBoard getQuestionById(Long id) {
        return webBoardRepo.findById(id).orElseThrow(QuestionNotAlreadyException::new);
    }

    @Override
    public List<WebBoard> getAllQuestion() {
        return (List<WebBoard>) webBoardRepo.findAll();
    }

    @Override
    public void deleteQuestion(Long id) {
        webBoardRepo.deleteById(id);
    }

    @Override
    public WebBoard addScoreLike(WebBoardDto webBoardDto, Long id) {
        WebBoard userQuestion = webBoardRepo.findById(id).orElseThrow(QuestionNotAlreadyException::new);
        userQuestion.setScore_like(userQuestion.getScore_like() + 1);
        return webBoardRepo.save(userQuestion);
    }

    @Override
    public List<WebBoard> getAllQuestionIsPublic() {
        return webBoardRepo.findByIsPublic(true);
    }

    @Override
    public List<WebBoard> getAllQuestionByAnswerNull() {
        return webBoardRepo.findByAnswerNull();
    }

    @Override
    public List<WebBoard> getAllQuestionByAnswerNotNull() {
        return webBoardRepo.findByAnswerNotNull(false);
    }

    @Override
    public List<WebBoard> getAllQuestionByIsPublicAndScoreLike() {
        return webBoardRepo.findByScoreLike(true, Sort.by("score_like").descending());
    }


}
