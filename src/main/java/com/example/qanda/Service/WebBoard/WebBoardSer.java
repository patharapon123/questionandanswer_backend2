package com.example.qanda.Service.WebBoard;

import com.example.qanda.Entity.WebBoard.WebBoard;
import com.example.qanda.Entity.WebBoard.WebBoardDto;

import java.util.List;

public interface WebBoardSer {

    WebBoard addQuestion(WebBoardDto webBoardDto);

    WebBoard addAnswer(WebBoardDto webBoardDto, Long id);

    WebBoard getQuestionById(Long id);

    List<WebBoard> getAllQuestion();

    void deleteQuestion(Long id);

    WebBoard addScoreLike(WebBoardDto webBoardDto, Long id);

    List<WebBoard> getAllQuestionIsPublic();

    List<WebBoard> getAllQuestionByAnswerNull();

    List<WebBoard> getAllQuestionByAnswerNotNull();

    List<WebBoard> getAllQuestionByIsPublicAndScoreLike();


}
