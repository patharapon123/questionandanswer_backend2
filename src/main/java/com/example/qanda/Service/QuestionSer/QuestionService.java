package com.example.qanda.Service.QuestionSer;

import com.example.qanda.Entity.Question.Question;
import com.example.qanda.Entity.Question.QuestionDto;

import java.util.List;

public interface QuestionService {

    Question addQuestion(QuestionDto questionDto);

    List<Question> getAllQuestion();

    Question updateQuestion(QuestionDto questionDto, Long id);

    void deleteQuestion(Long id);

    Question getQuestionById(Long id);

    List<Question> getAllQuestionIsPublic();

    List<Question> getAllQuestionTopic1();

    List<Question> getAllQuestionTopic2();

    List<Question> getAllQuestionTopic3();

    List<Question> getAllQuestionTopic4();

    List<Question> getAllQuestionTopic5();

    List<Question> getAllQuestionTopic6();

    List<Question> getAllQuestionTopic1AndIsPublic();

    List<Question> getAllQuestionTopic2AndIsPublic();

    List<Question> getAllQuestionTopic3AndIsPublic();

    List<Question> getAllQuestionTopic4AndIsPublic();

    List<Question> getAllQuestionTopic5AndIsPublic();

    List<Question> getAllQuestionTopic6AndIsPublic();

}
