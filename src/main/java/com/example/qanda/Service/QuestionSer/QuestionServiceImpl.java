package com.example.qanda.Service.QuestionSer;

import com.example.qanda.Entity.Admin.AdminRepo;
import com.example.qanda.Entity.Question.Question;
import com.example.qanda.Entity.Question.QuestionDto;
import com.example.qanda.Entity.Question.QuestionRepo;
import com.example.qanda.Exception.Question.QuestionNotAlreadyException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuestionServiceImpl implements QuestionService {

    @Autowired
    QuestionRepo questionRepo;

    @Autowired
    AdminRepo adminRepo;


    @Override
    public Question addQuestion(QuestionDto questionDto) {
        Question q = new Question();
        q.setTopic(questionDto.getTopic());
        q.setQuestion(questionDto.getQuestion());
        q.setAnswer(questionDto.getAnswer());
        q.setIsPublic(questionDto.getIsPublic());
        q.setAddDate(questionDto.getAddDate());
        q.setUpdateDate(questionDto.getUpdateDate());
        q.setLastUpdater(questionDto.getLastUpdater());
        return questionRepo.save(q);

    }

    @Override
    public List<Question> getAllQuestion() {
        return (List<Question>) questionRepo.findAll();

    }

    @Override
    public Question updateQuestion(QuestionDto questionDto, Long id) {
        Question question = questionRepo.findById(id).orElseThrow(QuestionNotAlreadyException::new);
        question.setTopic(questionDto.getTopic());
        question.setQuestion(questionDto.getQuestion());
        question.setAnswer(questionDto.getAnswer());
        question.setIsPublic(questionDto.getIsPublic());
        question.setAddDate(questionDto.getAddDate());
        question.setUpdateDate(questionDto.getUpdateDate());
        question.setLastUpdater(questionDto.getLastUpdater());
        return questionRepo.save(question);
    }

    @Override
    public void deleteQuestion(Long id) {
        questionRepo.deleteById(id);
    }

    @Override
    public Question getQuestionById(Long id) {
        return questionRepo.findById(id).orElseThrow(QuestionNotAlreadyException::new);
    }

    @Override
    public List<Question> getAllQuestionIsPublic() {
        return questionRepo.findByIsPublic(true);
    }

    @Override
    public List<Question> getAllQuestionTopic1() {
        return questionRepo.findByTopic("ด้านการบริการยืม-คืน");
    }

    @Override
    public List<Question> getAllQuestionTopic2() {
        return questionRepo.findByTopic("ด้านบริการอินเตอร์เน็ต");
    }

    @Override
    public List<Question> getAllQuestionTopic3() {
        return questionRepo.findByTopic("ด้านบริการห้องค้นคว้ากลุ่ม และห้องมัลติมีเดียกลุ่ม");
    }

    @Override
    public List<Question> getAllQuestionTopic4() {
        return questionRepo.findByTopic("ด้านบริการวารสาร หนังสือพิมพ์ นิตยสาร และบทความวารสาร");
    }

    @Override
    public List<Question> getAllQuestionTopic5() {
        return questionRepo.findByTopic("ด้านบริการสื่อโสต และบริการNetflix");
    }

    @Override
    public List<Question> getAllQuestionTopic6() {
        return questionRepo.findByTopic("Research Support Service");
    }

    @Override
    public List<Question> getAllQuestionTopic1AndIsPublic() {
        return questionRepo.findByTopicAndIsPublic("ด้านการบริการยืม-คืน", true);
    }

    @Override
    public List<Question> getAllQuestionTopic2AndIsPublic() {
        return questionRepo.findByTopicAndIsPublic("ด้านบริการอินเตอร์เน็ต", true);
    }

    @Override
    public List<Question> getAllQuestionTopic3AndIsPublic() {
        return questionRepo.findByTopicAndIsPublic("ด้านบริการห้องค้นคว้ากลุ่ม และห้องมัลติมีเดียกลุ่ม", true);
    }

    @Override
    public List<Question> getAllQuestionTopic4AndIsPublic() {
        return questionRepo.findByTopicAndIsPublic("ด้านบริการวารสาร หนังสือพิมพ์ นิตยสาร และบทความวารสาร", true);
    }

    @Override
    public List<Question> getAllQuestionTopic5AndIsPublic() {
        return questionRepo.findByTopicAndIsPublic("ด้านบริการสื่อโสต และบริการNetflix", true);
    }

    @Override
    public List<Question> getAllQuestionTopic6AndIsPublic() {
        return questionRepo.findByTopicAndIsPublic("Research Support Service", true);
    }


}
