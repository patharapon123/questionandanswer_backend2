package com.example.qanda.Entity.WebBoard;


import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@ToString
@Data
@Table(name = "UserQuestion")
public class WebBoard {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(name = "ID")
    public Long id;

    @NotNull
    @Column(name = "Question")
    public String question;

    @Column(name = "Question_Date")
    public String questionDate;

    @Column(name = "Answer")
    public String answer;

    @Column(name = "Answer_Date")
    public String answerDate;

    @Column(name = "Updater_User")
    public String updater_user;

    @Column(name = "Updater_Admin")
    public String updater_admin;

    @Column(name = "IS_Public")
    public Boolean isPublic = false;

    @Column(name = "Score_Like")
    public Long score_like = 0L;


}
