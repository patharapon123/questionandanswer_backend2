package com.example.qanda.Entity.WebBoard;

import lombok.Data;

@Data

public class WebBoardDto {
    public Long id;
    public String question;
    public String questionDate;
    public String answer;
    public String answerDate;
    public String updater_admin;
    public String updater_user;
    public Boolean isPublic = false;
    public Long score_like;
}

