package com.example.qanda.Entity.WebBoard;


import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface webBoardRepo extends CrudRepository<WebBoard, Long> {

    @Query("select w from WebBoard w where (w.answer IS NOT NULL and w.isPublic = :isPublic)")
    List<WebBoard> findByIsPublic(@Param("isPublic") Boolean isPublic);

    @Query("select w from WebBoard w where (w.answer IS NULL)")
    List<WebBoard> findByAnswerNull();

    @Query("select w from WebBoard w where (w.answer IS NOT NULL and w.isPublic = :isPublic)")
    List<WebBoard> findByAnswerNotNull(@Param("isPublic") Boolean isPublic);

    @Query("select w from WebBoard w where (w.isPublic = :isPublic and w.answer IS NOT NULL)")
    List<WebBoard> findByScoreLike(@Param("isPublic") Boolean isPublic, Sort sort);


}
