package com.example.qanda.Entity.Admin;

import lombok.Data;

@Data
public class AdminDto {

    public Long id;
    public String username;
    public String fullname;
    public String password;
    public String email;
    public String google_id;
    public Long thumbnail;
}
