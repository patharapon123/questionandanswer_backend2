package com.example.qanda.Entity.Admin;

import org.springframework.data.repository.CrudRepository;

public interface AdminRepo extends CrudRepository<Admin, Long> {

    Admin findByEmail(String email);

    Admin findByUsername(String username);


}
