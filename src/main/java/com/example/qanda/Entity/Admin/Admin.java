package com.example.qanda.Entity.Admin;


import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@ToString
@Data
@Entity
@Table(name = "ADMIN")
public class Admin {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(name = "ID")
    public Long id;

    @NotNull
    @Column(name = "USERNAME", unique = true)
    public String username;

    @Column(name = "FULLNAME")
    public String fullname;

    @NotNull
    @Column(name = "PASSWORD")
    public String password;

    @NotNull
    @Column(name = "EMAIL", unique = true)
    public String email;

    @Column(name = "GOOGLE_ID")
    public String google_id;

    @Column(name = "THUMBNAIL")
    public Long thumbnail;

}
