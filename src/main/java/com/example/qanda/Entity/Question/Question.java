package com.example.qanda.Entity.Question;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@ToString
@Entity
@Data
@Table(name = "QUESTION_ANSWER")
public class Question {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(name = "ID")
    public Long id;

    @NotNull
    @Column(name = "TOPIC")
    public String topic;


    @Lob
    @NotNull
    @Column(name = "QUESTION")
    public String question;

    @Lob
    @Column(name = "ANSWER")
    public String answer;

    @NotNull
    @Column(name = "IS_PUBLIC")
    public Boolean isPublic = true;

    @NotNull
    @Column(name = "ADD_DATE")
    public String addDate;

    @Column(name = "UPDATE_DATE")
    public String updateDate;

    @Column(name = "LAST_UPDATER")
    public String lastUpdater;


}
