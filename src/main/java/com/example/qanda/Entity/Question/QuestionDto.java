package com.example.qanda.Entity.Question;

import lombok.Data;


@Data
public class QuestionDto {

    public Long id;
    public String topic;
    public String question;
    public String answer;
    public Boolean isPublic = true;
    public String addDate;
    public String updateDate;
    public String lastUpdater;
}
