package com.example.qanda.Entity.Question;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface QuestionRepo extends CrudRepository<Question, Long> {

    @Query("select q from Question q where (q.isPublic = :isPublic)")
    List<Question> findByIsPublic(@Param("isPublic") Boolean isPublic);

    @Query("select q from Question q where (q.topic = :topic)")
    List<Question> findByTopic(@Param("topic") String topic);

    @Query("select q from Question q where (q.topic = :topic and q.isPublic = :isPublic )")
    List<Question> findByTopicAndIsPublic(@Param("topic") String topic, Boolean isPublic);


}
