package com.example.qanda.Entity.EmailDto;

import lombok.Data;

@Data
public class MailRequest {

    private String name;
    private String to;
    private String from;
    private String subject;
    private String answer;
    private String question;
}
