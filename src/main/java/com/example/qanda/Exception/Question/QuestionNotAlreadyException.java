package com.example.qanda.Exception.Question;

public class QuestionNotAlreadyException extends RuntimeException {

    public QuestionNotAlreadyException() {super("Question is not already");}
}
