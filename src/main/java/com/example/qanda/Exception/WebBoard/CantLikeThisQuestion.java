package com.example.qanda.Exception.WebBoard;

public class CantLikeThisQuestion extends RuntimeException {

    public CantLikeThisQuestion() {super("You already like this question");}
}
