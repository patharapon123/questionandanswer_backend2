package com.example.qanda.Exception.Admin;

public class UsernameDuplicate extends RuntimeException{

    public UsernameDuplicate() {
        super("username duplicate");
    }
}
