package com.example.qanda.Exception.Admin;

public class EmailDuplicate extends Exception {

    public EmailDuplicate() {
        super("email duplicate");
    }
}
