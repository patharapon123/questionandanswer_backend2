package com.example.qanda.Exception.Admin;

public class UnknownUserException extends RuntimeException {

    public UnknownUserException() {
        super("username and password incorrect");
    }
}
