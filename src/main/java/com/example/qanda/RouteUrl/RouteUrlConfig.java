package com.example.qanda.RouteUrl;

public class RouteUrlConfig {


    public interface Question {
        String QUESTION_MAPPING_NAME = "/question";
        String ADD_QUESTION = "/addQuestion";
        String GET_ALL_QUESTION = "/questions";
        String EDIT_QUESTION = "/editQuestion/{id}";
        String DELETE_QUESTION = "/deleteQuestion/{id}";
        String GET_QUESTION_BY_ID = "/questions/{id}";
        String GET_QUESTION_PUBLIC = "/questions/public";
        String GET_QUESTION_BY_TOPIC1 = "/questions/topic1";
        String GET_QUESTION_BY_TOPIC2 = "/questions/topic2";
        String GET_QUESTION_BY_TOPIC3 = "/questions/topic3";
        String GET_QUESTION_BY_TOPIC4 = "/questions/topic4";
        String GET_QUESTION_BY_TOPIC5 = "/questions/topic5";
        String GET_QUESTION_BY_TOPIC6 = "/questions/topic6";
        String GET_QUESTION_BY_TOPIC1_PUBLIC = "/questions/public/topic1";
        String GET_QUESTION_BY_TOPIC2_PUBLIC = "/questions/public/topic2";
        String GET_QUESTION_BY_TOPIC3_PUBLIC = "/questions/public/topic3";
        String GET_QUESTION_BY_TOPIC4_PUBLIC = "/questions/public/topic4";
        String GET_QUESTION_BY_TOPIC5_PUBLIC = "/questions/public/topic5";
        String GET_QUESTION_BY_TOPIC6_PUBLIC = "/questions/public/topic6";
    }


    public interface Admin {
        String ADMIN_MAPPING_NAME = "/admin";
        String LOGIN = "/login";
        String GET_ALL_ADMIN = "/admins";
        String GET_ADMIN_BY_ID = "/admins/{id}";
        String ADD_ADMIN = "/add";
        String EDIT_ADMIN = "/edit/{id}";
        String DELETE_ADMIN = "/delete/{id}";

    }


    public interface WebBoard {
        String WEBBOARD_MAPPING_NAME = "/webBoard";

        String GET_ALL_QUESTION = "/question";
        String GET_QUESTION_IS_PUBLIC = "/question/public";
        String GET_QUESTION_BY_ID = "/question/{id}";
        String GET_QUESTION_BY_ANSWER_NULL = "question/answerNull";
        String GET_QUESTION_BY_ANSWER_NOTNULL = "question/answerNotNull";
        String GET_QUESTION_BY_SCORE_LIKE_AND_IS_PUBLIC = "/question/public/score";

        //USER
        String USER_ADD_QUESTION = "/user/addQuestion";
        String USER_ADD_SCORE_LIKE = "/user/score/{id}";

        //ADMIN
        String ADMIN_ADD_ANSWER = "/admin/addAnswer/{id}";
        String ADMIN_DELETE_QUESTION = "/admin/delete/{id}";
    }


    public interface Email {
        String EMAIL_NAME = "/email";

        String SEND_EMAIL_ADD = "/send/add";
        String SEND_EMAIL_EDIT = "/send/edit";
    }
}
