package com.example.qanda.Controller;


import com.example.qanda.Entity.WebBoard.WebBoard;
import com.example.qanda.Entity.WebBoard.WebBoardDto;
import com.example.qanda.RouteUrl.RouteUrlConfig;
import com.example.qanda.Service.WebBoard.WebBoardSer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(RouteUrlConfig.WebBoard.WEBBOARD_MAPPING_NAME)

public class WebBoardController {

    @Autowired
    WebBoardSer webBoardSer;


    @PostMapping(RouteUrlConfig.WebBoard.USER_ADD_QUESTION)
    public WebBoard addUserQuestion(@RequestBody WebBoardDto webBoardDto) {
        return webBoardSer.addQuestion(webBoardDto);

    }

    @PostMapping(RouteUrlConfig.WebBoard.ADMIN_ADD_ANSWER)
    public WebBoard addAnswer(@RequestBody WebBoardDto webBoardDto, @PathVariable Long id) {
        return webBoardSer.addAnswer(webBoardDto, id);
    }

    @PostMapping(RouteUrlConfig.WebBoard.USER_ADD_SCORE_LIKE)
    public WebBoard addScore(@RequestBody WebBoardDto webBoardDto, @PathVariable Long id) {
        return webBoardSer.addScoreLike(webBoardDto, id);
    }

    @GetMapping(RouteUrlConfig.WebBoard.GET_ALL_QUESTION)
    public List<WebBoard> getAllUserQuestion() {
        return webBoardSer.getAllQuestion();
    }

    @GetMapping(RouteUrlConfig.WebBoard.GET_QUESTION_BY_ID)
    public WebBoard getUserQuestionById(@PathVariable Long id) {
        return
                webBoardSer.getQuestionById(id);
    }

    @GetMapping(RouteUrlConfig.WebBoard.GET_QUESTION_IS_PUBLIC)
    public List<WebBoard> getAllQuestionIsPublic() {

        return webBoardSer.getAllQuestionIsPublic();
    }

    @GetMapping(RouteUrlConfig.WebBoard.GET_QUESTION_BY_ANSWER_NULL)
    public List<WebBoard> getAllQuestionAnswerNull() {
        return webBoardSer.getAllQuestionByAnswerNull();
    }

    @GetMapping(RouteUrlConfig.WebBoard.GET_QUESTION_BY_ANSWER_NOTNULL)
    public List<WebBoard> getAllQuestionAnswerNotNull() {
        return webBoardSer.getAllQuestionByAnswerNotNull();
    }

    @GetMapping(RouteUrlConfig.WebBoard.GET_QUESTION_BY_SCORE_LIKE_AND_IS_PUBLIC)
    public List<WebBoard> getAllQuestionByIsPublicAndScoreLike() {
        return webBoardSer.getAllQuestionByIsPublicAndScoreLike();
    }


    @DeleteMapping(RouteUrlConfig.WebBoard.ADMIN_DELETE_QUESTION)
    public void deleteUserQuestion(@PathVariable Long id) {
        webBoardSer.deleteQuestion(id);
    }


}


