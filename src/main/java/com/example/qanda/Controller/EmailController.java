package com.example.qanda.Controller;

import com.example.qanda.Entity.EmailDto.MailRequest;
import com.example.qanda.Entity.EmailDto.MailResponse;
import com.example.qanda.RouteUrl.RouteUrlConfig;


import com.example.qanda.Service.EmailSer.EmailService;
import com.example.qanda.Service.EmailSer.EmailServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(RouteUrlConfig.Email.EMAIL_NAME)
public class EmailController {

    @Autowired
    private EmailService emailService;

    @PostMapping(RouteUrlConfig.Email.SEND_EMAIL_ADD)
    public MailResponse sendEmailAddAnswer(@RequestBody MailRequest request) {
        Map<String, Object> model = new HashMap<>();
        model.put("Question", request.getQuestion());
        model.put("Answer", request.getAnswer());
        return emailService.sendEmailAddAnswer(request, model);
    }

    @PostMapping(RouteUrlConfig.Email.SEND_EMAIL_EDIT)
    public MailResponse sendEmailEditAnswer(@RequestBody MailRequest request) {
        Map<String, Object> model = new HashMap<>();
        model.put("Name", request.getName());
        model.put("location", "Saraburi");
        return emailService.sendEmailEditAnswer(request, model);
    }
}
