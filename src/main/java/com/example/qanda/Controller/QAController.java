package com.example.qanda.Controller;

import com.example.qanda.Entity.Question.Question;
import com.example.qanda.Entity.Question.QuestionDto;
import com.example.qanda.RouteUrl.RouteUrlConfig;
import com.example.qanda.Service.QuestionSer.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(RouteUrlConfig.Question.QUESTION_MAPPING_NAME)
public class QAController {

    @Autowired
    QuestionService questionService;

    @PostMapping(RouteUrlConfig.Question.ADD_QUESTION)
    public Question addQuestion(@RequestBody QuestionDto questionDto) {
        return questionService.addQuestion(questionDto);
    }

    @GetMapping(RouteUrlConfig.Question.GET_ALL_QUESTION)
    public List<Question> getAllQuestion() {
        return questionService.getAllQuestion();
    }

    @DeleteMapping(RouteUrlConfig.Question.DELETE_QUESTION)
    public void deleteQuestion(@PathVariable Long id) {
        questionService.deleteQuestion(id);
    }

    @PostMapping(RouteUrlConfig.Question.EDIT_QUESTION)
    public Question editQuestion(@RequestBody QuestionDto questionDto, @PathVariable Long id) {

        return questionService.updateQuestion(questionDto, id);
    }

    @GetMapping(RouteUrlConfig.Question.GET_QUESTION_BY_ID)
    public Question getQuestionById(@PathVariable Long id) {
        return questionService.getQuestionById(id);
    }

    @GetMapping(RouteUrlConfig.Question.GET_QUESTION_PUBLIC)
    public List<Question> getAllQuestionIsPublic() {
        return questionService.getAllQuestionIsPublic();
    }

    @GetMapping(RouteUrlConfig.Question.GET_QUESTION_BY_TOPIC1)
    public List<Question> getQuestionByTopic1() {
        return questionService.getAllQuestionTopic1();
    }

    @GetMapping(RouteUrlConfig.Question.GET_QUESTION_BY_TOPIC2)
    public List<Question> getQuestionByTopic2() {
        return questionService.getAllQuestionTopic2();
    }

    @GetMapping(RouteUrlConfig.Question.GET_QUESTION_BY_TOPIC3)
    public List<Question> getQuestionByTopic3() {
        return questionService.getAllQuestionTopic3();
    }

    @GetMapping(RouteUrlConfig.Question.GET_QUESTION_BY_TOPIC4)
    public List<Question> getQuestionByTopic4() {
        return questionService.getAllQuestionTopic4();
    }

    @GetMapping(RouteUrlConfig.Question.GET_QUESTION_BY_TOPIC5)
    public List<Question> getQuestionByTopic5() {
        return questionService.getAllQuestionTopic5();
    }

    @GetMapping(RouteUrlConfig.Question.GET_QUESTION_BY_TOPIC6)
    public List<Question> getQuestionByTopic6() {
        return questionService.getAllQuestionTopic6();
    }

    @GetMapping(RouteUrlConfig.Question.GET_QUESTION_BY_TOPIC1_PUBLIC)
    public List<Question> getQuestionByTopic1IsPublic() {
        return questionService.getAllQuestionTopic1AndIsPublic();
    }

    @GetMapping(RouteUrlConfig.Question.GET_QUESTION_BY_TOPIC2_PUBLIC)
    public List<Question> getQuestionByTopic2IsPublic() {
        return questionService.getAllQuestionTopic2AndIsPublic();
    }

    @GetMapping(RouteUrlConfig.Question.GET_QUESTION_BY_TOPIC3_PUBLIC)
    public List<Question> getQuestionByTopic3IsPublic() {
        return questionService.getAllQuestionTopic3AndIsPublic();
    }

    @GetMapping(RouteUrlConfig.Question.GET_QUESTION_BY_TOPIC4_PUBLIC)
    public List<Question> getQuestionByTopic4IsPublic() {
        return questionService.getAllQuestionTopic4AndIsPublic();
    }

    @GetMapping(RouteUrlConfig.Question.GET_QUESTION_BY_TOPIC5_PUBLIC)
    public List<Question> getQuestionByTopic5IsPublic() {
        return questionService.getAllQuestionTopic5AndIsPublic();
    }

    @GetMapping(RouteUrlConfig.Question.GET_QUESTION_BY_TOPIC6_PUBLIC)
    public List<Question> getQuestionByTopic6IsPublic() {
        return questionService.getAllQuestionTopic6AndIsPublic();
    }


}
