package com.example.qanda.Controller;

import com.example.qanda.Entity.Admin.Admin;
import com.example.qanda.Entity.Admin.AdminDto;
import com.example.qanda.Exception.Admin.EmailDuplicate;
import com.example.qanda.RouteUrl.RouteUrlConfig;
import com.example.qanda.Service.AdminSer.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(RouteUrlConfig.Admin.ADMIN_MAPPING_NAME)
public class Admin_User_Controller {

    @Autowired
    AdminService adminService;


    @GetMapping(RouteUrlConfig.Admin.GET_ALL_ADMIN)
    public List<Admin> getAllAdmin() {
        return adminService.getAllAdmin();
    }

    @GetMapping(RouteUrlConfig.Admin.GET_ADMIN_BY_ID)
    public Admin GetAdminById(@PathVariable Long id) {
        return adminService.getAdminById(id);
    }


    @PostMapping(RouteUrlConfig.Admin.ADD_ADMIN)
    public Admin addAdmin(@RequestBody AdminDto adminDto) throws RuntimeException, EmailDuplicate {
        return adminService.addAdmin(adminDto);
    }

    @PostMapping(RouteUrlConfig.Admin.LOGIN)
    public String Login(@RequestBody AdminDto adminDto) throws RuntimeException {
        return adminService.login(adminDto);
    }

    @PostMapping(RouteUrlConfig.Admin.EDIT_ADMIN)
    public Admin editAdmin(@RequestBody AdminDto adminDto, @PathVariable Long id) throws RuntimeException {
        return adminService.editUser(adminDto, id);

    }

    @DeleteMapping(RouteUrlConfig.Admin.DELETE_ADMIN)
    public void deleteAdmin(@PathVariable Long id) {
        adminService.deleteAdmin(id);

    }

}
